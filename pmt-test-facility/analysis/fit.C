// C++ inc
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>

// ROOT inc
#include "TROOT.h"
#include "TSystem.h"
#include "TFile.h"
#include "TTree.h"
#include "TPaveStats.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TH1F.h"
#include "TF1.h"
#include "TMath.h"
#include "TSpectrum.h"
#include "TChain.h"
#include "TAxis.h"
#include "TLatex.h"

// RooFit inc
#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooGaussian.h"
#include "RooConstVar.h"
#include "RooAddPdf.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "RooProduct.h"
#include "RooFormulaVar.h"
#include "RooFitResult.h"
#include "RooCustomizer.h"
#include "RooFFTConvPdf.h"

using namespace RooFit;

#define DIM 8
#define NPX DIM*DIM

// Fwd declaration
void dofit( TString, Int_t, Int_t, TString );
RooFitResult* getFit( TH1F*, Int_t, TString );
//void loadHamamatsuGain( TString serialNb );

// Output file
TFile *outFile;

// Hamamatsu gains
Float_t HamamatsuGain[NPX]; 

// Output branches
Int_t m_npx = NPX;
Int_t m_status[NPX] = {0};
Float_t m_chi2[NPX] = {0.};
Float_t m_navgph[NPX] = {0.}, m_dnavgph[NPX] = {0.};   
Float_t m_navgct[NPX] = {0.}, m_dnavgct[NPX] = {0.};   
Float_t m_mean_n[NPX] = {0.}, m_dmean_n[NPX] = {0.};   
Float_t m_sigma_n[NPX] = {0.}, m_dsigma_n[NPX] = {0.};   
Float_t m_mean_1ph[NPX] = {0.}, m_dmean_1ph[NPX] = {0.};   
Float_t m_sigma_1ph[NPX] = {0.}, m_dsigma_1ph[NPX] = {0.};   
Float_t m_mean_1ct[NPX] = {0.}, m_dmean_1ct[NPX] = {0.};   
Float_t m_sigma_1ct[NPX] = {0.}, m_dsigma_1ct[NPX] = {0.};   
Float_t m_Pmiss[NPX] = {0.}, m_dPmiss[NPX] = {0.};   
Float_t m_fitTime[NPX] = {0.};

// output file prefix
TString prefix = "fit";


//======================================================
// Multigauss main routine
//======================================================
void fit(TString datadir, 
	 TString dataFile, 
	  Int_t pmtID,
           TString serialNb,
           Int_t pxID = -1 )
{
  /* Usage:
   1) Compile 
     .L fit.C+
   2) Runs on pixel 12 of MaPMT1
      fit("datadir","filename.root", 1, "FA0123", 12)
   3) Runs on all the available pixels of MaPMT1
      fit("datadir","filename.root", 1, "FA0123", -1)
   */

  // Does not show TCanvas, etc...
  gROOT->SetBatch();

  // Silent RooFit (stdout warnings/errors only)
  RooMsgService::instance().setGlobalKillBelow( RooFit::WARNING );


  //=================
  // Sanity checks
  //=================
  if (pxID != -1 && (pxID < 1 || pxID > NPX)) {
    cout << "[ERROR] Wrong pixel number! Exit" << endl;
    return;
  }
  if (gSystem->AccessPathName(dataFile)) {
    cout << "File " << dataFile << " does not exist! Exit!" << endl;
    return;
  }
  if (pmtID != 1 && pmtID != 2) {
    cout << "[ERROR] Wrong MaPMT number! Exit" << endl;
    return;
  }
  cout << "[INFO] Analyzing data file: " << dataFile << "..." << endl;
  cout << "[INFO] Analyzing MaPMT nb: " << pmtID << "..." << endl;
  cout << "[INFO] Analyzing pixel nb: " << ((pxID == -1) ? "all" : TString(pxID)) << "..." << endl;


  //=======================
  // Get Hamamatsu gains
  //=======================
  //loadHamamatsuGain( serialNb );

  // Get prefix according to input filename and serial number
  TObjArray *tokens = dataFile.Tokenize(TString("/"));
  if (tokens->GetEntries() > 0) {
    TString fileName = ((TObjString*)tokens->Last())->String();
    tokens = fileName.Tokenize(TString(".root"));
    if (tokens->GetEntries() > 0)
      prefix =  ((TObjString*)tokens->First())->String();
  }
  prefix = serialNb + "-" + prefix + Form("-pmt%i", pmtID);
  if (pxID != -1) prefix += Form("-px%i", pxID);

  // Create output file and tree 
  outFile = new TFile(datadir+"/fit-" + prefix + ".root", "recreate"); 
  TTree *tree = new TTree("tree", "");

  // Set branches
  tree->Branch("npx", &m_npx, "npx/I");

  tree->Branch("HamGain", &HamamatsuGain, "HamGain[npx]/F");

  tree->Branch("status", &m_status, "status[npx]/I");
  tree->Branch("chi2", &m_chi2, "chi2[npx]/F");

  tree->Branch("navgph", &m_navgph, "navgph[npx]/F");
  tree->Branch("dnavgph", &m_dnavgph, "dnavgph[npx]/F");

  tree->Branch("navgct", &m_navgct, "navgct[npx]/F");
  tree->Branch("dnavgct", &m_dnavgct, "dnavgct[npx]/F");

  tree->Branch("mean_n", &m_mean_n, "mean_n[npx]/F");
  tree->Branch("dmean_n", &m_dmean_n, "dmean_n[npx]/F");

  tree->Branch("sigma_n", &m_sigma_n, "sigma_n[npx]/F");
  tree->Branch("dsigma_n", &m_dsigma_n, "dsigma_n[npx]/F");

  tree->Branch("mean_1ph", &m_mean_1ph, "mean_1ph[npx]/F");
  tree->Branch("dmean_1ph", &m_dmean_1ph, "dmean_1ph[npx]/F");

  tree->Branch("sigma_1ph", &m_sigma_1ph, "sigma_1ph[npx]/F");
  tree->Branch("dsigma_1ph", &m_dsigma_1ph, "dsigma_1ph[npx]/F");

  tree->Branch("mean_1ct", &m_mean_1ct, "mean_1ct[npx]/F");
  tree->Branch("dmean_1ct", &m_dmean_1ct, "dmean_1ct[npx]/F");

  tree->Branch("sigma_1ct", &m_sigma_1ct, "sigma_1ct[npx]/F");
  tree->Branch("dsigma_1ct", &m_dsigma_1ct, "dsigma_1ct[npx]/F");

  tree->Branch("Pmiss", &m_Pmiss, "Pmiss[npx]/F");
  tree->Branch("dPmiss", &m_dPmiss, "dPmiss[npx]/F");
  
  tree->Branch("fitTime", &m_fitTime, "fitTime[npx]/F");

  // Start!
  time_t start, end;
  time( &start );

  // Fit only one px 
  if (pxID != -1 ) {
    dofit( dataFile, pmtID, pxID, datadir );
  }

  // Fit all pixels
  else {
    for (Int_t px = 1; px <= NPX; px++) {  
      dofit( dataFile, pmtID, px, datadir ); 
    }
  }

  // Stop!
  time( &end );
  int timing = difftime( end, start );

  // Close output file
  outFile->cd();
  tree->Fill();
  tree->Write();
  outFile->Close();
  
  // Print timing
  cout << endl;
  cout << "[INFO] Fitting time: " << int(timing / 60) << " min" 
       << " and " << timing % 60 << " sec" << endl;

  cout << "[INFO] ----- End of program -----" << endl;
  cout << endl;
}


//======================================================
// Prepare dataset
//======================================================
void dofit( TString dataFile, 
	    Int_t pmtID,
            Int_t pxID,
	    TString pdfdir="./"
	    )
{
  gROOT->cd();

  TH1F *hdata;
  Int_t nEvents = -1;

  cout << "[INFO] Processing pixel: " << pxID << " of MaPMT: " << pmtID << endl;


  //==================
  // Load MAROC data
  //==================
  // Set chain
  TChain ch("mdfTree");
  ch.Add(dataFile);
  nEvents = ch.GetEntries(); 

  // ADC for pixels
  TString branchName = Form("B01_PMT%i_ADC", pmtID);
  UShort_t PMT_ADC[64]; ch.SetBranchAddress(branchName, &PMT_ADC[0]);

  // ADC for MAROC channel
  //UShort_t B01_MAR1_ADC[64]; ch.SetBranchAddress("B01_MAR1_ADC",&B01_MAR1_ADC[0]);
  //UShort_t B01_MAR2_ADC[64]; ch.SetBranchAddress("B01_MAR2_ADC",&B01_MAR2_ADC[0]);

  // Set data histogram
  Double_t x_min = ch.GetMinimum(branchName); 
  Double_t x_max = ch.GetMaximum(branchName);
  if (x_max > 350.) x_max = 350.;
  if (x_min < -20.) x_min = -20.;
  Int_t nBins = x_max - x_min;
  hdata = new TH1F("hdata", "", nBins, x_min, x_max);

  // Event loop
  for (Int_t i = 0; i < nEvents; i++) {
    ch.GetEvent(i); 
    if (float(PMT_ADC[pxID-1]) > 11) { //HACK!!!  
      hdata->Fill( float(PMT_ADC[pxID-1]) ); 
    }
  }//evts

  cout << "[INFO] Number of events: " << nEvents << endl;

  RooFitResult* result =  getFit( hdata, pxID, pdfdir );
}
  

//======================================================
// Perform the fit
//======================================================
RooFitResult* getFit( TH1F *hdata, int pxID, TString pdfdir)
{
  gROOT->cd();

  // Start!
  time_t start, end;
  time( &start );


  //====================
  // Fit configuration
  //====================
  // Do not fit (overlays model to data using initial parameters)
  const bool dryrun = false;

  // max. number of photons
  const int Nphotons = 2;
  
  // max. number of cross-talk events
  const int Ncrosstalk = 2;

  // max. number of missing dynodes
  const int Nmissed = 6;  


  //======================
  // Define the dataset
  //======================
  hdata->SetMinimum(1.);
  Double_t x_min = hdata->GetXaxis()->GetXmin(); 
  Double_t x_max = hdata->GetXaxis()->GetXmax(); 

  RooRealVar x("x", "x", x_min, x_max);
  x.setBins(hdata->GetNbinsX(), "cache"); // nBins too aggressive?
  RooDataHist dh("dh", "dh", x, Import(*hdata));


  //=================
  // Peaks finding
  //=================
  cout << "[INFO] Searching peaks..." << endl;

  TCanvas *cPeaks = new TCanvas("cPeaks", "Peaks finding", 600, 600); 
  cPeaks->SetLogy();

  TSpectrum *s = new TSpectrum(4);
  Double_t thr = 1. / hdata->GetBinContent(hdata->GetMaximumBin()); 	
 
  TH1F* hsmooth = (TH1F*)hdata->Clone("hsmooth"); // smooth the histogram!
  hsmooth->Smooth(); 

  Int_t npeaks = s->Search(hdata, 3, " ", thr); 
  //Int_t npeaks = s->Search(hsmooth, 3, " ", thr); // use smoothed histogram!
#if ROOT_VERSION_CODE >= ROOT_VERSION(6,00,0)
  Double_t *x_peaks = s->GetPositionX(); // ROOT6 needs Double_t!!!
  Double_t *y_peaks = s->GetPositionY();
#else
  Float_t *x_peaks = s->GetPositionX(); // ROOT6 needs Double_t!!!
  Float_t *y_peaks = s->GetPositionY();
#endif

  cout << "[INFO] Found " << npeaks << " candidate peaks !" << endl;
  if (npeaks < 2) {
    cout << "[ERROR] Just 1 peak found! Abort!" << endl;
    m_status[pxID -1] = -1;
    return 0;
  } 

  
  //===========================
  // Estimate starting params
  //===========================
  cout << "[INFO] Estimating initial parameters..." << endl;

  Double_t lowe = 0.5*TMath::Abs(x_peaks[0]); 
  Double_t uppe = 1.5*TMath::Abs(x_peaks[0]);

  TF1 *noiseFun = new TF1("noiseFun", "gaus(0)", x_min, x_max);
  TF1 *sigFun = new TF1("sigFun", "gaus(0)", x_min, x_max);
  TF1 *ctFun = new TF1("ctFun", "gaus(0)", x_min, x_max);
  TF1 *totFun = new TF1("totFun", "gaus(0) + gaus(3)", x_min, x_max);

  // Fit 1pe peak!
  cout << "\n[INFO] Fitting 1pe peak..." << endl;
  sigFun->SetParLimits(0, 0.001*hdata->GetEntries(), 0.3*hdata->GetEntries());
  sigFun->SetParameter(0, 0.1*hdata->GetEntries());
  sigFun->SetParLimits(1, 0.95*x_peaks[1], 1.05*x_peaks[1]);  
  sigFun->SetParameter(1, x_peaks[1]);
  sigFun->SetParLimits(2, sqrt(x_peaks[1]), 4.*sqrt(x_peaks[1])); // >>>>>> EMPIRICAL FORMULA!!!
  sigFun->SetParameter(2, 2.*sqrt(x_peaks[1]));
  hdata->Fit("sigFun", "N", "e", 0.7*x_peaks[1], 1.3*x_peaks[1]); 

  // Fit noise with one gaussian!
  cout << "\n[INFO] Fitting pedestal with 1 gaussian (just noise)..." << endl;
  noiseFun->SetParLimits(0, 0.1*hdata->GetEntries(), hdata->GetEntries());
  noiseFun->SetParameter(0, 0.5*hdata->GetEntries());
  noiseFun->SetParLimits(1, 0.95*x_peaks[0], 1.05*x_peaks[0]);  
  noiseFun->SetParameter(1, x_peaks[0]);
  noiseFun->SetParLimits(2, 0.01*sqrt(x_peaks[0]), 0.04*sqrt(x_peaks[0]));  // >>>>>> EMPIRICAL FORMULA!!!
  noiseFun->SetParameter(2, 0.02*sqrt(x_peaks[0]));
  hdata->Fit("noiseFun", "N", "e", 0.7*x_peaks[0], 1.3*x_peaks[0]); 

  Double_t mean_n0 = noiseFun->GetParameter(1);            // noise mean
  Double_t sigma_n0 = noiseFun->GetParameter(2);           // noise width
  Double_t mean_ct0 = 0.02 * (x_peaks[1] - x_peaks[0]);   // ct mean       // >>>>>> EMPIRICAL FORMULA!!!

  // Then, fit noise with two gaussians!
  cout << "\n[INFO] Fitting pedestal with 2 gaussians (noise + cross-talk)..." << endl;
  totFun->SetParLimits(0, 0.1*hdata->GetEntries(), hdata->GetEntries());
  totFun->SetParameter(0, noiseFun->GetParameter(0));      // norm noise 

  totFun->SetParLimits(1, 0.95*mean_n0, 1.05*mean_n0);     // mean noise
  totFun->SetParameter(1, mean_n0);                        
  
  totFun->SetParLimits(2, 0.7*sigma_n0, 1.3*sigma_n0);      // sigma noise 
  totFun->SetParameter(2, sigma_n0);                       
  
  totFun->SetParLimits(3, 0.1*noiseFun->GetParameter(0), noiseFun->GetParameter(0));  // norm. ct
  totFun->SetParameter(3, 0.3*noiseFun->GetParameter(0));  
  
  totFun->SetParLimits(4, mean_n0 + 0.5*mean_ct0, mean_n0 + 1.5*mean_ct0);   // mean ct
  totFun->SetParameter(4, mean_n0 + mean_ct0);             
  
  totFun->SetParLimits(5, 0.5*mean_ct0, 1.5*mean_ct0);      // sigma ct // >>>>>> EMPIRICAL FORMULA!!! (sqrt(s_ct**2 + s_n**2) ~ 0.02*mean_1ph ~ mean_ct)
  totFun->SetParameter(5, mean_ct0);                        

  hdata->Fit("totFun", "N", "e", lowe, uppe); 
  noiseFun->SetParameters(totFun->GetParameter(0), totFun->GetParameter(1), totFun->GetParameter(2));
  ctFun->SetParameters(totFun->GetParameter(3), totFun->GetParameter(4), totFun->GetParameter(5));

  // Final values
  mean_n0 = noiseFun->GetParameter(1);                     // noise mean
  sigma_n0 = noiseFun->GetParameter(2);                    // noise width

  Double_t mean_1ph0 = sigFun->GetParameter(1) - mean_n0;  // 1pe mean
  if (mean_1ph0 < 0.) mean_1ph0 = x_peaks[1];
  
  Double_t sigma_1ph0 = sigFun->GetParameter(2);           // 1pe width 
  sigma_1ph0 = (sigma_1ph0 > sigma_n0) ? sqrt(sigma_1ph0*sigma_1ph0 - sigma_n0*sigma_n0) : sigma_n0;
  
  mean_ct0 = ctFun->GetParameter(1) - mean_n0;             // ct mean
  if (mean_ct0 < 0.) mean_ct0 = mean_n0;
  
  Double_t sigma_ct0 = ctFun->GetParameter(2);             // ct width 
  sigma_ct0 = (sigma_ct0 > sigma_n0) ? sqrt(sigma_ct0*sigma_ct0 - sigma_n0*sigma_n0) : sigma_n0;
  if (sigma_ct0 < 1.) sigma_ct0 = 1.0; // otherwise norm is negative!!! (why???)
  
  Double_t Nped = hdata->Integral(hdata->FindBin(lowe), hdata->FindBin(uppe)); 
  Double_t navgph0 = Nped / hdata->GetEntries();           // number p.e.
  navgph0 = -log(navgph0);
  
  Double_t navgct0 = ctFun->Integral(lowe, uppe);          // number ct p.e.
  navgct0 /= noiseFun->Integral(lowe, uppe); 
  navgct0 = log(1. + navgct0);

  // Extra check on cross talk estimation 
  cout << "[INFO] Cross-talks estimated params.: " << navgct0 << " " << mean_ct0 << " " << sigma_ct0 << endl;
  if (navgct0 > 2.*navgph0) navgct0 = navgph0;

  sigFun->Draw("same");
  totFun->Draw("same");

  // Dump canvas on .pdf
  bool only_one_px = prefix.Contains("px");
  if (only_one_px) {
    cPeaks->SaveAs(pdfdir+"/peaks-" + prefix + ".pdf");
  }
  else {
    if (pxID == 1) cPeaks->SaveAs(pdfdir+"/peaks-" + prefix + ".pdf[");
    if (pxID >= 1 && pxID <= NPX) cPeaks->SaveAs(pdfdir+"/peaks-" + prefix + ".pdf");
    if (pxID == NPX) cPeaks->SaveAs(pdfdir+"/peaks-" + prefix + ".pdf]"); 
  }


  //=========================
  // Define fit parameters
  //=========================
  // mean and sigma of noise
  RooRealVar mean_n("mean_n", "mean of noise gaussians", mean_n0, 0.7*mean_n0, 1.3*mean_n0); // well known... narrow range... 
  RooRealVar sigma_n("sigma_n", "width of noise gaussians", sigma_n0, 0.5*sigma_n0, 1.5*sigma_n0); 

  // mean and sigma of single photon response
  RooRealVar mean_1ph("mean_1ph", "mean of single photon gaussian", mean_1ph0, 0.7*mean_1ph0, 1.3*mean_1ph0);
  RooRealVar sigma_1ph("sigma_1ph", "width of single photon gaussian", sigma_1ph0, 0.7*sigma_1ph0, 1.3*sigma_1ph0); 

  // average number of incident photons on pixel
  RooRealVar navgph("navgph", "mean number of incident photons per pixel", navgph0, 0.7*navgph0, 1.3*navgph0);

  // mean and sigma of 1 p.e. component for cross talk
  RooRealVar mean_1ct("mean_1ct", "mean of 1pe cross-talk gaussian", mean_ct0, 0.5*mean_ct0, 1.5*mean_ct0); // poorly known... wider range...
  RooRealVar sigma_1ct("sigma_1ct", "width of 1pe cross-talk gaussian", sigma_ct0, 0.5*sigma_ct0, 1.5*sigma_ct0); 

  // mean number of cross talk signals per pixels
  RooRealVar navgct("navgct", "mean number of cross-talk photons per pixel", navgct0, 0.5*navgct0, 1.5*navgct0); 

  // probability of missing a dynode
  Double_t Pmiss0 = 0.15;
  RooRealVar Pmiss("Pmiss", "Prob. missing a dynode", Pmiss0, 0.7*Pmiss0, 1.3*Pmiss0); // poorly known... wider range...
  
  // fix paramaters
  navgct.setConstant(true);
  mean_1ct.setConstant(true);
  sigma_1ct.setConstant(true);
  //Pmiss.setConstant(true);
  

  //=====================
  // Build noise model
  //=====================
  RooAbsPdf *noise_pdf = new RooGaussian("noise_pdf", "Noise pdf", x, mean_n, sigma_n); 


  //======================
  // Build signal model
  //======================
  TString name, expr, title;
  RooArgList *pdfs = new RooArgList;
  RooArgList *coeffs = new RooArgList;
  RooAbsPdf *signal_pdf[Nphotons];

  // gi = G**1/N * (ri**k / (r1*r2)**k/N) = G**1/N * g0
  Double_t g0 = 1.0;
  const Int_t Ndynodes = 12;
  const Double_t r1 = 2.3;
  const Double_t r2 = 1.2;
  const Double_t k = 2./3.;
  const Double_t pcorr = 0.3;
  const Double_t conv = 1.0; // 0.45;

  // Build single photon response
  for (int nmiss = 0; nmiss <= Nmissed; nmiss++) {

    // Gain reduction factor (g1*g2*g3*...)
    if (nmiss == 1) g0 *= pow(r1, k) / pow(r1*r2, k/Ndynodes) / conv;
    if (nmiss == 2) g0 *= pow(r2, k) / pow(r1*r2, k/Ndynodes) / conv;
    if (nmiss > 2)  g0 *= 1. / pow(r1*r2, k/Ndynodes) / conv;

    name.Form("gmiss_dy%i", nmiss);
    expr.Form("%f * TMath::Power(mean_1ph, %f)", g0, Double_t(nmiss)/Ndynodes);
    RooFormulaVar *gmiss = new RooFormulaVar(name, expr, RooArgList(mean_1ph)); 

    //std::cout << "[INFO] g" << nmiss << ": " << gmiss->getVal() << std::endl;

    // mean
    name.Form("mean_1ph_dy%i", nmiss);
    expr.Form("mean_1ph / %s", gmiss->GetName());
    RooFormulaVar *mean = new RooFormulaVar(name, expr, RooArgList(mean_1ph, *gmiss));

    // sigma
    name.Form("sigma_1ph_dy%i", nmiss);
    expr.Form("sigma_1ph / TMath::Sqrt(%s)", gmiss->GetName());
    RooFormulaVar *sigma = new RooFormulaVar(name, expr, RooArgList(sigma_1ph, *gmiss));

    // gaussian 
    name.Form("pdf_1ph_dy%i", nmiss);
    title.Form("Gaussian for %i missing dynodes", nmiss);
    RooAbsPdf *gauss = new RooGaussian(name, title, x, *mean, *sigma); 

    // coefficient
    name.Form("N_1ph_dy%i", nmiss); 
    if (nmiss == 0) expr.Form("(1. - Pmiss - %i*%f*Pmiss)", Nmissed-1, pcorr); 
    if (nmiss == 1) expr.Form("Pmiss"); 
    if (nmiss > 1)  expr.Form("Pmiss * %f", pcorr); 

    RooFormulaVar *coeff = new RooFormulaVar(name, expr, RooArgList(Pmiss));
    pdfs->add( *gauss );
    coeffs->add( *coeff );
  }

  // Build pdf for multiple photons
  for (int nph = 1; nph <= Nphotons; nph++) {
    name.Form("signal0_pdf_%i", nph);
    title.Form("%i photons signal pdf", nph);

    if (nph == 1) signal_pdf[0] = new RooAddPdf(name, title, *pdfs, *coeffs);
    else signal_pdf[nph-1] = new RooFFTConvPdf(name, title, x, *(signal_pdf[nph-2]), *(signal_pdf[0]));
  }

  // add some noise :-)
  for (int nph = 1; nph <= Nphotons; nph++) {
    name.Form("signal_pdf_%i", nph);
    title.Form("%i photons signal pdf", nph);
    signal_pdf[nph-1] = new RooFFTConvPdf(name, title, x, *(signal_pdf[nph-1]), *noise_pdf);
  }
  

  //==========================
  // Build cross-talk model
  //==========================
  RooAbsPdf *crosstalk_pdf[Ncrosstalk];
  RooAbsPdf *crosstalk0_pdf[Ncrosstalk];

  for (int nct = 1; nct <= Ncrosstalk; nct++) {
    // mean
    name.Form("mean_ct_%i", nct);
    expr.Form("mean_n + %i*mean_1ct", nct);
    RooFormulaVar *mean = new RooFormulaVar(name, expr, RooArgList(mean_n, mean_1ct));

    // sigma
    name.Form("sigma_ct_%i", nct);
    expr.Form("TMath::Sqrt(sigma_n*sigma_n + %i*sigma_1ct*sigma_1ct)", nct);
    RooFormulaVar *sigma = new RooFormulaVar(name, expr, RooArgList(sigma_n, sigma_1ct));

    // gaussian 
    name.Form("crosstalk_pdf_%i", nct);
    title.Form("%i cross-talks pdf", nct);
    crosstalk_pdf[nct-1] = new RooGaussian(name, title, x, *mean, *sigma); 
    
    //========= no noise =========
    // mean
    name.Form("mean0_ct_%i", nct);
    expr.Form("%i*mean_1ct", nct);
    mean = new RooFormulaVar(name, expr, RooArgList(mean_1ct));

    // sigma
    name.Form("sigma0_ct_%i", nct);
    expr.Form("TMath::Sqrt(%i)*sigma_1ct", nct);
    sigma = new RooFormulaVar(name, expr, RooArgList(sigma_1ct));

    // gaussian 
    name.Form("crosstalk0_pdf_%i", nct);
    title.Form("%i cross-talks pdf", nct);
    crosstalk0_pdf[nct-1] = new RooGaussian(name, title, x, *mean, *sigma); 
    //============================
  }


  //====================
  // Build full model 
  //====================
  RooArgList *model_pdfs = new RooArgList;
  RooArgList *model_coeffs = new RooArgList;
  RooAbsPdf *signal_crosstalk_pdf[Nphotons][Ncrosstalk];

  for (int nph = 0; nph <= Nphotons; nph++) {
    for (int nct = 0; nct <= Ncrosstalk; nct++) {
      RooAbsPdf *pdf;
      name.Form("pdf_%iph_%ict", nph, nct);
      title.Form("%i photons + %i cross-talks pdf", nph, nct);

      // noise only
      if (!nph && !nct) pdf = noise_pdf;

      // signal only
      else if (nph && !nct) pdf = signal_pdf[nph-1];

      // cross-talk only
      else if (!nph && nct) pdf = crosstalk_pdf[nct-1];

      // signal + cross-talk
      else {
	pdf = new RooFFTConvPdf(name, title, x, *(signal_pdf[nph-1]), *(crosstalk0_pdf[nct-1]));
	signal_crosstalk_pdf[nph-1][nct-1] = pdf;
      }

      // normalization
      name.Form("coeff_%iph_%ict", nph, nct); 
      expr.Form("TMath::Poisson(%i, navgph) * TMath::Poisson(%i, navgct)", nph, nct); 
      RooFormulaVar *coeff = new RooFormulaVar(name, expr, RooArgList(navgph, navgct));

      // add to the model
      model_pdfs->add( *pdf );
      model_coeffs->add( *coeff );
    }
  }

  RooAddPdf model("model", "Spectrum model", *model_pdfs, *model_coeffs);


  //================
  // Fit spectrum
  //================
  cout << "[INFO] Fit spectrum!" << endl;

  RooFitResult *fitres = NULL;
  if (!dryrun) {
    fitres = model.fitTo(dh, Save(true), NumCPU(4));
    fitres->SetName( Form("fitresult_px%i", pxID) );
  }


  //==============================
  // Print and plot fit results
  //==============================
  // Plot components
  TCanvas* cFit = new TCanvas("cFit", Form("Fit to spectrum of pixel %i", pxID), 1000, 500);

  RooPlot* frame = x.frame( Title(Form("pixel %i", pxID)) );
  frame->SetName( Form("frame_px%i", pxID) );
  dh.plotOn( frame );
  model.plotOn(frame, "", LineColor(kBlue), NumCPU(4)); // full model

  Double_t norm = TMath::Poisson(0, navgph.getVal()) * TMath::Poisson(0, navgct.getVal());
  noise_pdf->plotOn(frame, Components("noise_pdf"), LineColor(kBlack), LineStyle(kSolid), Normalization(norm), NumCPU(4)); // pedestal

  for (int nph = 1; nph <= Nphotons; nph++) { // signal
    Double_t norm = TMath::Poisson(nph, navgph.getVal()) * TMath::Poisson(0, navgct.getVal());
    signal_pdf[nph-1]->plotOn(frame, "", LineColor(nph+1), LineStyle(kSolid), Normalization(norm), NumCPU(4));
  }

  for (int nct = 1; nct <= Ncrosstalk; nct++) { // cross-talk
    Double_t norm = TMath::Poisson(0, navgph.getVal())* TMath::Poisson(nct, navgct.getVal());
    crosstalk_pdf[nct-1]->plotOn(frame, "", LineColor(nct+1), LineStyle(kDashed), Normalization(norm), NumCPU(4));
  }

  for (int nph = 1; nph <= Nphotons; nph++) { // signal + cross-talk
    for (int nct = 1; nct <= Ncrosstalk; nct++) {
      Double_t norm = TMath::Poisson(nph, navgph.getVal())* TMath::Poisson(nct, navgct.getVal());
      signal_crosstalk_pdf[nph-1][nct-1]->plotOn(frame, "", LineColor(nph+1), LineStyle(kDashed), Normalization(norm), NumCPU(4));
    }
  }

  //cFit->SetLogy();
  frame->SetMinimum( 1. );
  frame->SetMaximum( 1.5*y_peaks[1] );
  frame->Draw();

  TLatex caption; // captions
  caption.SetTextSize(0.04);
  caption.DrawLatex(0.8*x_max, 1.2*y_peaks[1], Form("Pixel %i", pxID));

  // Print peaks
  cout << endl;
  cout << "TSpectrum results:" << endl;
  s->Print();

  // Print difference wrt initial values
  cout << endl;
  cout << "Fitted values wrt initial estimation: " << endl;
  cout << "mean_n = "    << 100.*(mean_n.getVal() - mean_n0) / mean_n0           << "%"  << endl;
  cout << "mean_1ph = "  << 100.*(mean_1ph.getVal() - mean_1ph0) / mean_1ph0     << "%"  << endl;
  cout << "mean_1ct = "  << 100.*(mean_1ct.getVal() - mean_ct0) / mean_ct0       << "%"  << endl;
  cout << "sigma_1ph = " << 100.*(sigma_1ph.getVal() - sigma_1ph0) / sigma_1ph0  << "%"  << endl;
  cout << "sigma_n = "   << 100.*(sigma_n.getVal() - sigma_n0) / sigma_n0        << "%"  << endl;
  cout << "sigma_ct = "  << 100.*(sigma_1ct.getVal() - sigma_ct0) / sigma_ct0    << "%"  << endl;
  cout << "navgph = "    << 100.*(navgph.getVal() - navgph0) / navgph0           << "%"  << endl;
  cout << "navgct = "    << 100.*(navgct.getVal() - navgct0) / navgct0           << "%"  << endl;
  cout << "Pmiss = "     << 100.*(Pmiss.getVal() - Pmiss0) / Pmiss0              << "%"  << endl;
  cout << endl;

  // Dump canvas on .pdf
  if (only_one_px) {
    cFit->SaveAs(pdfdir+"/fit-" + prefix + ".pdf");
  }
  else {
    if (pxID == 1) cFit->SaveAs(pdfdir+"/fit-" + prefix + ".pdf[");
    if (pxID >= 1 && pxID <= NPX) cFit->SaveAs(pdfdir+"/fit-" + prefix + ".pdf");
    if (pxID == NPX) cFit->SaveAs(pdfdir+"/fit-" + prefix + ".pdf]"); 
  }

  // Exit if dryrun...
  if (dryrun) return 0;

  // Print fitted values
  if (fitres != NULL) {
    fitres->Print(); 
    fitres->floatParsFinal().Print("s") ;
  }
  cout << "chi2 = " << frame->chiSquare() << endl;
 
  // Check fit status
  Int_t status = 0;
  Double_t threshold = (0.1 / 100.); // == 0.1 %

  // navgph
  if (TMath::Abs( navgph.getVal()/navgph.getMin()-1. ) < threshold || 
      TMath::Abs( navgph.getVal()/navgph.getMax()-1. ) < threshold) status++;
  // navgct
  if (TMath::Abs( navgct.getVal()/navgct.getMin()-1. ) < threshold ||
      TMath::Abs( navgct.getVal()/navgct.getMax()-1. ) < threshold) status++;
  // mean_n
  if (TMath::Abs( mean_n.getVal()/mean_n.getMin()-1. ) < threshold ||
      TMath::Abs( mean_n.getVal()/mean_n.getMax()-1. ) < threshold) status++;
  // sigma_n
  if (TMath::Abs( sigma_n.getVal()/sigma_n.getMin()-1. ) < threshold || 
      TMath::Abs( sigma_n.getVal()/sigma_n.getMax()-1. ) < threshold) status++;
  // mean_1ph
  if (TMath::Abs( mean_1ph.getVal()/mean_1ph.getMin()-1. ) < threshold ||
      TMath::Abs( mean_1ph.getVal()/mean_1ph.getMax()-1. ) < threshold) status++;
  // sigma_1ph
  if (TMath::Abs( sigma_1ph.getVal()/sigma_1ph.getMin()-1. ) < threshold ||
      TMath::Abs( sigma_1ph.getVal()/sigma_1ph.getMax()-1. ) < threshold) status++;
  // mean_1ct
  if (TMath::Abs( mean_1ct.getVal()/mean_1ct.getMin()-1. ) < threshold ||
      TMath::Abs( mean_1ct.getVal()/mean_1ct.getMax()-1. ) < threshold) status++;
  // sigma_1ct  
  if (TMath::Abs( sigma_1ct.getVal()/sigma_1ct.getMin()-1. ) < threshold ||
      TMath::Abs( sigma_1ct.getVal()/sigma_1ct.getMax()-1. ) < threshold) status++;
  // Pmiss
  if (TMath::Abs( Pmiss.getVal()/Pmiss.getMin()-1. ) < threshold ||
      TMath::Abs( Pmiss.getVal()/Pmiss.getMax()-1. ) < threshold ) {
    if (status) status++;
    else status = -1;
  }

  cout << "status = " << status << endl;
  
  // Stop!
  time( &end );


  //==============
  // Fill NTuple 
  //==============
  m_chi2[pxID -1] = frame->chiSquare();
  m_status[pxID -1] = status;

  m_navgph[pxID -1] = navgph.getVal();
  m_dnavgph[pxID -1] = navgph.getPropagatedError(*fitres);

  m_navgct[pxID -1] = navgct.getVal();
  m_dnavgct[pxID -1] = navgct.getPropagatedError(*fitres);
  
  m_mean_n[pxID -1] = mean_n.getVal();
  m_dmean_n[pxID -1] = mean_n.getPropagatedError(*fitres);
  
  m_sigma_n[pxID -1] = sigma_n.getVal();
  m_dsigma_n[pxID -1] = sigma_n.getPropagatedError(*fitres);

  m_mean_1ph[pxID -1] = mean_1ph.getVal();
  m_dmean_1ph[pxID -1] = mean_1ph.getPropagatedError(*fitres);

  m_sigma_1ph[pxID -1] = sigma_1ph.getVal();
  m_dsigma_1ph[pxID -1] = sigma_1ph.getPropagatedError(*fitres);

  m_mean_1ct[pxID -1] = mean_1ct.getVal();
  m_dmean_1ct[pxID -1] = mean_1ct.getPropagatedError(*fitres);

  m_sigma_1ct[pxID -1] = sigma_1ct.getVal();
  m_dsigma_1ct[pxID -1] = sigma_1ct.getPropagatedError(*fitres);
  
  m_Pmiss[pxID -1] = Pmiss.getVal();
  m_dPmiss[pxID -1] = Pmiss.getPropagatedError(*fitres);

  m_fitTime[pxID -1] = difftime( end, start );

  outFile->cd();
  fitres->Write();
  frame->Write();

  return fitres;
}

